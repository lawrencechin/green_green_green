<h1><span style="color: #232;">Green</span>, <span style="color= #264">Green</span> & <span style="color: #286;">Green</span></h1>

![main image](main_image.jpg)

A small website about the colour *green* featuring poetry and plasticine. I think this was for an assignment in the distant past but my memory is fuzzy; particularly the last part regarding *Backdoor Santa*… The main motivation, I believe, was to play around with plasticine.

Here's a slightly modified version for perusal -> [link](https://lawrencechin.gitlab.io/green_green_green/)

![secondary image](secondary_image.jpg)
